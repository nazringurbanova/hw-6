package com.company;

import java.util.Arrays;

public class Human{
    private String name;
    private String surname;
    private int year;
    private int iq;
    public com.company.Pet pet;
    private Human mother;
    private Human father;
    String[][] schedule = new String[7][5];
    public Human(){

    }

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public void greetPet() {
        System.out.println("Hello" +  pet.getnickname());
    }

    public void describePet(){
        if (pet.getage()>50)
            System.out.println("I have a " + pet.getSpecies() +", he is "+pet.getage()+" years old, he is very sly");
        else
            System.out.println("I have a " + pet.getSpecies() +", he is "+pet.getage()+" years old, he is almost not sly");
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", pet=" + pet +
                ", mother=" + mother +
                ", father=" + father +
                ", schedule=" + Arrays.deepToString(schedule) + //for 2d
                '}';
    }
}